export class Division {
    id: number;
    parent_id: number;
    adminHistory: string;
    code: string;
    dateBegin: string;
    dateEnd: string;
    functions: string;
    level: string;
    num: string;
    scopeContent: string;
}

