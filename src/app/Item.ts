export class Item {

    box: String;
    dateBegin: Date;
    dateEnd: Date;
    folder: String;
    language: String;
    level: String;
    num: String;
    scopeContent: String;
    title: String;

}