export class Subseries {
    id: number;
    parent_id: number;
    arrangement: string;
    dateBegin: string;
    dateEnd: string;
    extentDescription: string;
    extentSize: string;
    language: string;
    num: string;
    scopeContent: string;
    level: string;
    title: string;
}