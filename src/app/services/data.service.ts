import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class DataService {

  constructor(private http: HttpClient) { }

  /*************************
   * P O S T   M A P P I N G
   *************************/

  postDepartment(allTogether) {
    this.http.post("http://localhost:8080/catalogue/post/department", allTogether).subscribe(
      //condition ? expr1 : expr2 
      data => data != null ? console.log(data) : null,
      err => err != null ? console.log(err) : null
    );
  }
  postDivision(allTogether) {
    this.http.post("http://localhost:8080/catalogue/post/division", allTogether).subscribe(
      //condition ? expr1 : expr2 
      data => data != null ? console.log(data) : null,
      err => err != null ? console.log(err) : null
    );
  }
  postSeries(allTogether) {
    this.http.post("http://localhost:8080/catalogue/post/series", allTogether).subscribe(
      //condition ? expr1 : expr2 
      data => data != null ? console.log(data) : null,
      err => err != null ? console.log(err) : null
    );
  }
  postSubseries(allTogether) {
    this.http.post("http://localhost:8080/catalogue/post/subseries", allTogether).subscribe(
      //condition ? expr1 : expr2 
      data => data != null ? console.log(data) : null,
      err => err != null ? console.log(err) : null
    );
  }
  postItem(allTogether) {
    this.http.post("http://localhost:8080/catalogue/post/item", allTogether).subscribe(
      //condition ? expr1 : expr2 
      data => data != null ? console.log(data) : null,
      err => err != null ? console.log(err) : null
    );
  }

  /***********************
   * G E T   M A P P I N G
  /***********************/

  getEverything() {
    return this.http.get('http://localhost:8080/catalogue/get/everything');
  }

  getId(input) {
    return this.http.get('http://localhost:8080/catalogue/get/' + input);
  }

  getLevel(input) {
    return this.http.get('http://localhost:8080/catalogue/get/level/' + input);
  }

  getByParentId(input) {
    return this.http.get('http://localhost:8080/catalogue/get/parentId/' + input);
  }



  /*****************************
   * D E L E T E   M A P P I N G
   /****************************/

  deleteById(id) {
    return this.http.delete("http://localhost:8080/catalogue/delete/" + id);
  }

}

