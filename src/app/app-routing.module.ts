import { CatalogueComponent } from './components/catalogue/catalogue.component';
import { FileParsingComponent } from './components/file-parsing/file-parsing.component';
import { HomeComponent } from './components/home/home.component';
import { ItemFormComponent } from './components/item-form/item-form.component';
import { ItemViewerComponent } from './components/item-viewer/item-viewer.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'catalogue',
    component: CatalogueComponent
  },
  {
    path: 'catalogue/item-viewer',
    component: ItemViewerComponent
  },
  {
    path: 'item-form',
    component: ItemFormComponent
  },
  {
    path: 'file-parsing',
    component: FileParsingComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
