export class Series {
    id: number;
    parent_id: number;
    access: string;
    adminHistory: string;
    arrangement: string;
    dateBegin: string;
    dateEnd: string;
    extentDescription: string;
    extentSize: string;
    language: string;
    notes: string;
    num: string;
    level: string;
    scopeContent: string;
    title: string;
}