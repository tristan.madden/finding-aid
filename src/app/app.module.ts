import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule} from '@angular/forms';
import { PapaParseModule } from 'ngx-papaparse';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { ItemViewerComponent } from './components/item-viewer/item-viewer.component';
import { ItemFormComponent } from './components/item-form/item-form.component';
import { HomeComponent } from './components/home/home.component';
import { CatalogueComponent } from './components/catalogue/catalogue.component';
import { FileParsingComponent } from './components/file-parsing/file-parsing.component';

@NgModule({
  declarations: [
    AppComponent,
    SidebarComponent,
    ItemViewerComponent,
    ItemFormComponent,
    HomeComponent,
    CatalogueComponent,
    FileParsingComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    FormsModule,
    PapaParseModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { 
  
}
