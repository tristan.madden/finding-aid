import { Collection } from './collection';

export const COLLECTIONS: Collection[] = [
    { id: 0, name: "M-93-3-22-160", description: "Liliuokalani Collection", route: "item-viewer"},
    { id: 1, name: 'FOEX', description: "Inventory of Records of the Foreign Office & Executive", route: "home"}
];
