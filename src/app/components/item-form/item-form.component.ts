import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';

//import * as langugaes from '../../../resources/languages.json!json";

@Component({
  selector: 'app-form',
  templateUrl: './item-form.component.html',
  styleUrls: ['./item-form.component.scss']
})
export class ItemFormComponent implements OnInit {

  constructor(private data: DataService) { }

  languages = ["Mandarin", " Spanish", " English", " Hindi", " Arabic", " Portuguese", " Bengali", " Russian", " Japanese", " Punjabi", " German", " Javanese", " Wu", " Malay", " Telugu", " Vietnamese", " Korean", " French", " Marathi", " Tamil", " Urdu", " Turkish", " Italian", " Yue", " Thai", " Gujarati", " Jin", " Teochew", " Hokkien", " Persian", " Polish", " Pashto", " Kannada", " Xiang", " Malayalam", " Sundanese", " Hausa", " Odia", " Burmese", " Hakka", " Ukrainian", " Bhojpuri", " Tagalog", " Yoruba", " Maithili", " Uzbek", " Sindhi", " Amharic", " Fula", " Romanian", " Oromo", " Igbo", " Azerbaijani", " Awadhi", " Gan", " Chinese", " Cebuano", " Dutch", " Kurdish", " Serbian", " Croatian", " Malagasy", " Saraiki", " Nepali", " Sinhalese", " Chittagonian", " Zhuang", " Khmer", " Turkmen", " Assamese", " Madurese", " Somali", " Marwari", " Magahi", " Haryanvi", " Hungarian", " Chhattisgarhi", " Greek", " Chewa", " Deccan", " Akan", " Kazakh", " Min", " Sylheti", " Zulu", " Czech", " Kinyarwanda", " Dhundhari", " Haitian", " Creole", " Fuzhounese", " Ilocano", " Quechua", " Kirundi", " Swedish", " Hmong", " Shona", " Uyghur", " Hiligaynon", " Mossi", " Xhosa", " Belarusian", " Balochi", " Konkani", " Ilonggo", " Visayan", " Hawaiian"];

  onSubmit(a, b, c, d, e, f, g, h, i) {
    //this.data.createItem(a,b,c,d,e,f,g,h,i);
  }

  log(x) {
    console.log(x);
  }

  ngOnInit() {
  }

}