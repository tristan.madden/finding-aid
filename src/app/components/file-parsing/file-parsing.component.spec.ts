import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FileParsingComponent } from './file-parsing.component';

describe('FileParsingComponent', () => {
  let component: FileParsingComponent;
  let fixture: ComponentFixture<FileParsingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FileParsingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FileParsingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
