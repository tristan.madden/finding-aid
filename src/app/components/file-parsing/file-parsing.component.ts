import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';
import { PapaParseService } from 'ngx-papaparse';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'file-parsing',
  templateUrl: './file-parsing.component.html',
  styleUrls: ['./file-parsing.component.scss']
})

export class FileParsingComponent implements OnInit {

  isFileLoaded: Boolean = false; //Do not allow a DB operation if there is no file loaded!
  departmentDisabled: Boolean = false;
  divisionDisabled: Boolean = true;
  seriesDisabled: Boolean = true;
  subseriesDisabled: Boolean = true;
  itemDisabled: Boolean = true;
  buttonDisabled: Boolean = true;

  reference$: String[] = [];
  departments$: Object; //An array of DepartmentEntity Observables loaded from SQL
  divisions$: Object; //An array of DivisionEntity Observables loaded from SQL
  series$: Object; //An array of SeriesEntity Observables loaded from SQL
  subseries$: Object; //An array of SubseriesEntity Observables loaded from SQL
  items$: Object; //An array of items loaded from CSV

  parsedDepartments: Object;
  parsedDivisions: Object;
  parsedSeries: Object;
  parsedSubseries: Object;
  parsedItems: Object;

  level: number;

  constructor(private papa: PapaParseService, private data: DataService) {
  }

  ngOnInit() {
    console.log("ngOnInit()");
    //Get every entry from the database whose level is "department"
    this.data.getLevel("department").subscribe(data => this.departments$ = data);
    this.level = 0;
  }

  departmentSelected(f: NgForm) {
    console.log("departmentSelected()");
    this.divisionDisabled = false;
    this.data.getByParentId(f.value.department.id).subscribe(
      data => this.divisions$ = data
    );
    this.reference$[0] = f.form.value.department.name;
    this.level = 1;
  }

  divisionSelected(f: NgForm) {
    this.seriesDisabled = false;
    this.data.getByParentId(f.value.division.id).subscribe(
      data => this.series$ = data
    );
    this.reference$[1] = "/" + f.form.value.division.name;
    this.level = 2;
  }

  seriesSelected(f: NgForm) {
    this.subseriesDisabled = false;
    this.data.getByParentId(f.value.series.id).subscribe(
      data => this.subseries$ = data
    );
    this.reference$[2] = "/" + f.form.value.series.title;
    this.level = 3;
  }

  subseriesSelected(f: NgForm) {
    this.itemDisabled = false;
    this.reference$[3] = "/" + f.form.value.subseries.title;
    this.level = 4;
  }

  fileSelected() {
    this.buttonDisabled = false;
  }

  onSubmit(f: NgForm) {

    if (this.isFileLoaded) {
      switch (this.level) {
        case 0:
          this.sendDepartment(this.parsedDepartments, null, null, null, null);
          this.data.getLevel("department").subscribe(data => this.departments$ = data);
          break;
        case 1:
          this.sendDivision(f.value.department, this.parsedDivisions, null, null, null);
          this.data.getLevel("division").subscribe(data => this.departments$ = data);
          break;
        case 2:
          this.sendSeries(null, f.value.division, this.parsedSeries, null, null);
          this.data.getLevel("series").subscribe(data => this.series$ = data);
          break;
        case 3:
          this.sendSubseries(null, null, f.value.series, this.subseries$, null);
          this.data.getLevel("subseries").subscribe(data => this.subseries$ = data);
          break;
        case 4:
          this.sendItem(null, null, null, f.value.subseries, this.items$);
          this.data.getLevel("item").subscribe(data => this.items$ = data);
          break;
      }
    } else {
      console.error("You forgot to load a CSV file!");
    }
  }

  sendDepartment(_department, division, series, subseries, item) {
    for (let i = 0; i < _department.data.length; i++) {
      let department = _department.data[i];
      let allTogether = { department, division, series, subseries, item };
      console.log(allTogether);
      this.data.postDepartment(allTogether);
    }
  }

  sendDivision(department, _division, series, subseries, item) {
    for (let i = 0; i < _division.data.length; i++) {
      let division = _division.data[i];
      let allTogether = { department, division, series, subseries, item };
      console.log(allTogether);
      this.data.postDivision(allTogether);
    }
  }

  sendSeries(department, division, _series, subseries, item) {
    for (let i = 0; i < _series.data.length; i++) {
      let series = _series.data[i];
      let allTogether = { department, division, series, subseries, item };
      console.log(allTogether);
      this.data.postSeries(allTogether);
    }
  }

  sendSubseries(department, division, series, _subseries, item) {
    for (let i = 0; i < _subseries.data.length; i++) {
      let subseries = _subseries.data[i];
      let allTogether = { department, division, series, subseries, item };
      console.log(allTogether);
      this.data.postSeries(allTogether);
    }
  }

  sendItem(department, division, series, subseries, _item) {
    for (let i = 0; i < _item.data.length; i++) {
      let item = _item.data[i];
      let allTogether = { department, division, series, subseries, item };
      console.log(allTogether);
      this.data.postSeries(allTogether);
    }
  }

  changeListener(files: FileList) {
    console.log("changeListener()");
    this.isFileLoaded = true;
    //This is just the configuration for PapaParse!
    let config = {
      delimiter: ",",	// auto-detect
      newline: "",	// auto-detect
      quoteChar: '"',
      escapeChar: '"',
      header: true,
      trimHeaders: false,
      dynamicTyping: true,
      preview: 0,
      encoding: "",
      worker: false,
      step: undefined,
      complete: (result, file) => {
        switch (this.level) {
          case 0: this.parsedDepartments = result; break;
          case 1: this.parsedDivisions = result; break;
          case 2: this.parsedSeries = result; break;
          case 3: this.parsedSubseries = result; break;
          case 4: this.parsedItems = result; break;
        }
      },
      error: undefined,
      download: false,
      skipEmptyLines: false,
      chunk: undefined,
      fastMode: undefined,
      beforeFirstChunk: undefined,
      withCredentials: undefined,
      transform: undefined
    }
    /***********************************************
     * This is where the actual parsing takes place!
     ***********************************************/
    Array.from(files).forEach(file => {
      let reader: FileReader = new FileReader();
      reader.readAsText(file);
      reader.onload = (e) => {
        let csv: string | ArrayBuffer = reader.result;
        this.papa.parse(file, config);
      }
    });
  }

}