import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';
import { trigger, style, transition, animate, keyframes, query, stagger } from '@angular/animations';

@Component({
  selector: 'app-item',
  templateUrl: './item-viewer.component.html',
  styleUrls: ['./item-viewer.component.scss'],
  animations: [
    trigger('listStagger', [
      transition('* <=> *', [
        query(
          ':enter',
          [
            style({ opacity: 0, transform: 'translateY(-16px)' }),
            stagger(
              '100ms',
              animate(
                '100ms ease-out',
                style({ opacity: 1, transform: 'translateY(-8px)' })
              )
            )
          ],
          { optional: true }
        ),
        query(':leave', animate('50ms', style({ opacity: 0 })), {
          optional: true
        })
      ])
    ])
  ]
})
export class ItemViewerComponent implements OnInit {

  items$: Object;

  constructor(private data: DataService) { }

  ngOnInit() {

    this.data.getEverything().subscribe(
      data => this.items$ = data

    );

  }

  deleteThis() {
    //this.data.deleteById(this.items$);
  }

}
