import { Component, OnInit } from '@angular/core';
import { Collection } from '../../collection';
import { COLLECTIONS } from '../../mock-collection';

@Component({
  selector: 'app-catalogue',
  templateUrl: './catalogue.component.html',
  styleUrls: ['./catalogue.component.scss']
})
export class CatalogueComponent implements OnInit {
  collections = COLLECTIONS;

  selectedCollection: Collection;

  constructor() { }

  ngOnInit() {

  }

  onSelect(c: Collection): void {
    this.selectedCollection = c;
  }
}
