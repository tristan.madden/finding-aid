export class Department {
    id: number;
    parent_id: number;
    adminHistory: string;
    code: string;
    dateBegin: Date;
    dateEnd: Date;
    functions: string;
    level: string;
    name: string;
    scopeContent: string;
    title: string;
}